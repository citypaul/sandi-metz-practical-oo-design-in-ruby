# Chapter 2 #
 
## Managing dependencies ##
 * [Inject Dependencies](#Inject Dependencies)


### Inject Dependencies ###

 Use dependency injection - make classes more powerful by having them know less
 
 Commit: 96a2bbe
 If you cannot control the code that injects the dependency, isolate the creation of the
 object inside the class itself.
 
 Commit: 6a803fc
 Lazy load the new class - only create the wheel dependency internally if it is required by the class
 
 Still tightly coupled to the idea of using a wheel and not just using a duck type
 that contains a diameter method. This code is however now easier to refactor, and the
 dependencies are revealed.
 
 Isolate class name dependencies in this way.
 
**Isolate vulnerable external messages** 552d3664

After isolating external class names with dependency injection, we can also isolate our code from the names of external methods in the same way.

```ruby
def gear_inches
	ratio * wheel.diameter
end

```

While this is simple, the call to ``wheel.diameter`` could easily be deeply hidden within a more complex function. The same call could also be made in multiple methods as well, which means if the method ever changed on the wheel object, we would have to hunt through the code to discover all the usages of it. Isolating the method this way protects us from these issues:

```ruby
def diameter
	wheel.diameter
end
```

Then gear inches becomes:

```ruby
def gear_inches
	#lines of scary math
	foo = some_intermediate_result * diameter
	#more lines of scary math
end
```

##Remove argument order dependencies##

###Use Hashes for initialisation arguments - 6a8364c

If you can control the initialize method of an object, you can refactor it to take in a hash so that the order of arguments no longer matters

So the initialize method now looks like this:

```ruby
class Gear
  attr_reader :chainring, :cog, :wheel

  def initialize (args)
    @chainring = args[:chainring]
    @cog = args[:cog]
    @wheel = args[:wheel]
  end
  
  …
end

Gear.new(
    :chainring => 52,
    :cog => 11,
    :wheel => Wheel.new(26, 1.5)).gear_inches
```

###Setting default values- a9e7f91

Setting defaults can be done using ``||``, but this is not a good idea when booleans are involved. For example, the following expression sets ``@bool`` to ``true`` when ``:boolean_thing`` is missing *and* when it is present but set to ``false`` or ``nil``:

```ruby
@bool = args[:boolean_thing] || true
```

For this reason, you can use the ``fetch()`` method instead. In the below method, ``@chainring`` can be set to false by calling code:

```ruby
def initialize (args)
    @chainring = args.fetch(:chainring, 40)
    @cog = args.fetch(:cog, 18)
    @wheel = args[:wheel]
end
```

If the defaults are more complicated, you can create a defaults method that does the same thing
by using merge [302eaed]: 

```ruby
 def initialize (args)
    args = defaults.merge(args)
    @chainring = args[:chainring]
    @cog = args[:cog]
    @wheel = args[:wheel]
  end

  def defaults
    {:chainring => 40, :cog => 18}
  end
```