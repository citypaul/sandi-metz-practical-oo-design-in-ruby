require_relative 'wheel'
class Gear
  attr_reader :chainring, :cog, :wheel

  def initialize (args)
    args = defaults.merge(args)
    @chainring = args[:chainring]
    @cog = args[:cog]
    @wheel = args[:wheel]
  end

  def defaults
    {:chainring => 40, :cog => 18}
  end

  def ratio
    chainring / cog.to_f
  end

  def gear_inches
    #scary math
    ratio * diameter
    #scary math
  end

  def wheel
    @wheel ||= Wheel.new(@rim, @tire)
  end

  #providing this method isolates the dependency on the knowledge that
  #the Wheel class has a diameter method
  def diameter
    wheel.diameter
  end

end

puts Gear.new(
         :chainring => 52,
         :cog => 11,
         :wheel => Wheel.new(26, 1.5)).gear_inches
