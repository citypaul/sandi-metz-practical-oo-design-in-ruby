class ObscuringReferences

  attr_reader :data

  def initialize (data)
    @data = data
  end

  def diameters
    #diameters method requires intimate details of the structure of data in order
    #to convert it into wheels
    data.collect { |cell| cell[0] + cell[1] * 2 }
  end
end

#rim and tire sizes in a 2d array
puts ObscuringReferences.new([[622, 20], [622, 23], [559, 30], [559, 40]]).data

